# Servers

Most of the servers are based on services/TCP Protocols, and those are many. So we'll  focus on main things:
    - Web server
        - nginx
        - apache2
    - Database server
        - RDBMS:
            - sqlite
            - mysql
            - postgresql
        - NoSQL:
            - tinydb
            - redis
            - mongodb
    - Virualization
        - kvm
        - virtualbox
  
